from gym.envs.registration import register

register(
    id='icub_skin-v0',
    entry_point='gym_icub_skin.envs:IcubSkinEnv',
)

