% Read 2D coordinates of taxel projections
fileID = fopen('./coordinates/torso.txt', 'r');
skin = fscanf(fileID, '%f');
skin = reshape(skin, [3, numel(skin) / 3])';
fclose(fileID);

% Delete thermal pad taxels (every 7th and 11th taxel)
skin(7:12:end,:) = [];
skin(10:11:end,:) = [];

% Read error data
fileID = fopen('./data/disagreement-errors.txt', 'r');
err = fscanf(fileID, '%f');
err = reshape(err, [3, numel(err) / 3])';
fclose(fileID);

% Read generated goals
random_goals = readmatrix('./data/disagreement-goals.txt');

% Plot parameters
w = 500; % image width
h = 500; % image height
r = 15;  % scatter plot radius

% Make figure
figure
hold on
set(gca, 'YDir','reverse')

% Plot goals
scatter(random_goals(:,1),random_goals(:,2), r, [0.9, 0.9, 0.9], 'filled');

% Plot all taxels
scatter(skin(:,2),skin(:,3), r, 'black', 'filled');

% Highlight goal taxels
goal = 4;
scatter(skin(goal:10:end,2),skin(goal:10:end,3), r, 'magenta', 'filled');
% text(skin(goal:10:end,2),skin(goal:10:end,3), string(goal:10:440));

% Plot unreached taxels
for i = 1:size(err,1)
    row = err(i,:);
    if row(2) == 0
        scatter(skin(row(1),2),skin(row(1),3), r, 'red', 'filled');
    end
end

% Plot error circles
for i = 1:size(err,1)
    row = err(i,:);
    if row(2) == 1
        viscircles(skin(row(1),2:3), row(3) / 3, 'Color', 'magenta', 'Linewidth', 1);
    end
end

% Plot meta
xlim([0 w]);
ylim([0 h]);
xlabel('x [px]')
ylabel('y [px]')
title('Exploration by disagreement, iCub torso')
% legend('Random goals', 'Training taxels', 'Reached with error/3', 'Unreached', 'Location', 'southeast')
