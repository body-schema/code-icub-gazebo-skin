% Read error data
fileID = fopen('./data/goal-babbling-reaching-error.txt', 'r');
data1 = fscanf(fileID, '%f');
data1 = reshape(data1, [2, numel(data1) / 2])';
fclose(fileID);

fileID = fopen('./data/disagreement-reaching-error.txt', 'r');
data2 = fscanf(fileID, '%f');
data2 = reshape(data2, [2, numel(data2) / 2])';
fclose(fileID);

figure
hold on
plot(data1(:,1), data1(:,2), 'LineWidth', 2)
plot(data2(:,1), data2(:,2), 'LineWidth', 2)

% Plot meta
xlabel('Iteration number')
ylabel('Mean reaching error [px]')
title('Mean reaching error, iCub torso')
legend('Random goal babbling', 'Exploration by disagreement'); %, 'Location', 'southeast')
