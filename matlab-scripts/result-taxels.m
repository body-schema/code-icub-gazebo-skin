% Read error data
fileID = fopen('./data/goal-babbling-taxels.txt', 'r');
data1 = fscanf(fileID, '%f');
data1 = reshape(data1, [2, numel(data1) / 2])';
fclose(fileID);

fileID = fopen('./data/motor-babbling-taxels.txt', 'r');
data2 = fscanf(fileID, '%f');
data2 = reshape(data2, [2, numel(data2) / 2])';
fclose(fileID);

fileID = fopen('./data/disagreement-taxels.txt', 'r');
data3 = fscanf(fileID, '%f');
data3 = reshape(data3, [2, numel(data3) / 2])';
fclose(fileID);

figure
hold on
plot(data1(:,1), data1(:,2), 'LineWidth', 2)
plot(data2(:,1), data2(:,2), 'LineWidth', 2)
plot(data3(:,1), data3(:,2), 'LineWidth', 2)

% Plot meta
xlabel('Iteration number')
ylabel('Number of reached taxels')
title('Reached taxels, iCub torso')
legend('Random goal babbling', 'Random motor babbling', 'Exploration by disagreement', 'Location', 'northwest')
