% Read 2D coordinates of taxel projections
fileID = fopen('./coordinates/torso.txt', 'r');
skin = fscanf(fileID, '%f');
skin = reshape(skin, [3, numel(skin) / 3])';
fclose(fileID);

% Delete thermal pad taxels (every 7th and 11th taxel)
skin(7:12:end,:) = [];
skin(10:11:end,:) = [];

% Plot parameters
w = 500; % image width
h = 500; % image height
r = 15;  % scatter plot radius

% Plot all taxels
figure
hold on
set(gca, 'YDir','reverse')
scatter(skin(:,2),skin(:,3), r, 'black', 'filled');

% Highlight goal taxels
goal = 4;
scatter(skin(goal:10:end,2),skin(goal:10:end,3), r*2, 'blue', 'filled');
% text(skin(goal:10:end,2),skin(goal:10:end,3), string(goal:10:440));

% Draw edges
edges = [
    4 54
    14 24
    14 44
    34 44
    44 54
    54 64
    74 84
    84 94
    104 114
    124 134
    104 134
    134 144
    144 154
    154 164
    164 74
    164 174
    184 194
    94 254
    174 254
    34 374
    64 384
    374 384
    384 394
    394 404
    404 414
    414 424
    374 424
    404 434
    414 344
    344 354
    114 364
    204 364
    194 204
    124 174
    244 254
    244 274
    264 274
    124 264
    114 314
    264 314
    304 314
    294 304
    194 304
    184 334
    324 334
    294 324
    284 294
    274 284
    214 224
    214 244
    224 234
    234 284
]';
for edge = edges
    % plot([skin(edge(1),2) skin(edge(2),2)], [skin(edge(1),3) skin(edge(2),3)], 'Color', 'blue');
end

% Plot meta
xlim([0 w]);
ylim([0 h]);
xlabel('x [px]')
ylabel('y [px]')
title('Target goal grid, iCub torso')
legend('Skin taxels', 'Target goals', 'Location', 'southeast')
