% Read 2D coordinates of taxel projections
fileID = fopen('./coordinates/left_forearm.txt', 'r');
skin = fscanf(fileID, '%f');
skin = reshape(skin, [3, numel(skin) / 3])';
fclose(fileID);

% Delete thermal pad taxels (every 7th and 11th taxel)
skin(7:12:end,:) = [];
skin(10:11:end,:) = [];

% Plot parameters
w = 450; % image width
h = 600; % image height
r = 15;  % scatter plot radius

% Plot all taxels
figure
hold on
set(gca, 'YDir','reverse')
scatter(skin(:,2),skin(:,3), r, 'black', 'filled');

% Highlight goal taxels
goal = 4;
scatter(skin(goal:10:end,2),skin(goal:10:end,3), r * 2, 'blue', 'filled');
% text(skin(goal:10:end,2),skin(goal:10:end,3), string(goal:10:230));

% Draw edges
edges = [
    164 214
    214 224
    184 224
    184 194
    194 204
    174 204
    34 44
    44 54
    34 64
    64 74
    54 104
    94 104
    64 94
    84 94
    104 114
    84 134
    114 124
    124 134
    114 144
    144 154
    4 154
    4 14
    14 124
    14 24
]';
for edge = edges
    % plot([skin(edge(1),2) skin(edge(2),2)], [skin(edge(1),3) skin(edge(2),3)], 'Color', 'blue');
end

% Plot meta
xlim([0 w]);
ylim([0 h]);
xlabel('x [px]')
ylabel('y [px]')
title('Target goal grid, iCub forearm')
legend('Skin taxels', 'Target goals', 'Location', 'southeast')
