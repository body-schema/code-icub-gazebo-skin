% Read error data
fileID = fopen('./data/goal-babbling-reaching-error.txt', 'r');
data1 = fscanf(fileID, '%f');
data1 = reshape(data1, [2, numel(data1) / 2])';
fclose(fileID);

fileID = fopen('./data/model-wnn-reaching-error.txt', 'r');
data2 = fscanf(fileID, '%f');
data2 = reshape(data2, [2, numel(data2) / 2])';
fclose(fileID);

fileID = fopen('./data/model-lwlr-reaching-error.txt', 'r');
data3 = fscanf(fileID, '%f');
data3 = reshape(data3, [2, numel(data3) / 2])';
fclose(fileID);

figure
hold on
plot(data1(:,1), data1(:,2), 'LineWidth', 2)
plot(data2(:,1), data2(:,2), 'LineWidth', 2)
plot(data3(:,1), data3(:,2), 'LineWidth', 2)

% Plot meta
xlabel('Iteration number')
ylabel('Mean reaching error [px]')
title('Mean reaching error, iCub torso')
legend('NN', 'WNN', 'LWLR'); %, 'Location', 'southeast')
