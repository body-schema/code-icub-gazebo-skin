'use strict';

// Positions were obtained from /usr/share/iCub/contexts/skinGui/positions
const positionsPath = './positions/'

// Model file was obtained from /home/osboxes/catkin_ws/src/icub-models/build/iCub/robots/iCubGazeboV2_5_visuomanip/model.urdf
const modelFileName = './model.urdf';

// Configuration
const fs = require('fs');
const config = JSON.parse(fs.readFileSync('./config.json', 'utf-8'));

const calcRpy = ([x, y, z]) => {
  const roll = Math.atan2(z, y) + Math.PI / 2;
  const yaw = Math.atan2(x, (-y)) + Math.PI / 2
  const pitch = -Math.atan2(Math.sqrt(x*x + y*y), z)

  return [0, pitch, yaw];
};

const files = fs.readdirSync(positionsPath);
let skinXML = '';

// Generate self collision
[
  // 'chest', 'chest_skin',
  // 'l_shoulder_1', 'l_shoulder_2', 'l_shoulder_3',
  // 'l_upper_arm', 'l_elbow_1', 'l_wrist_1',
  
  // 'l_forearm', 'l_hand',
  
  // 'l_hand_index_0', 'l_hand_little_0', 'l_hand_middle_0', 'l_hand_ring_0', 'l_hand_thumb_0',
  // 'l_hand_index_1', 'l_hand_little_1', 'l_hand_middle_1', 'l_hand_ring_1', 'l_hand_thumb_1',
  // 'l_hand_index_2', 'l_hand_little_2', 'l_hand_middle_2', 'l_hand_ring_2', 'l_hand_thumb_2',
  'l_hand_index_3', 'l_hand_little_3', 'l_hand_middle_3', 'l_hand_ring_3', 'l_hand_thumb_3',
  
  // 'r_shoulder_1', 'r_shoulder_2', 'r_shoulder_3',
  // 'r_upper_arm', 'r_elbow_1', 'r_wrist_1', 
  
  // 'r_forearm', 'r_hand',
  
  // 'r_hand_index_0', 'r_hand_little_0', 'r_hand_middle_0', 'r_hand_ring_0', 'r_hand_thumb_0',
  // 'r_hand_index_1', 'r_hand_little_1', 'r_hand_middle_1', 'r_hand_ring_1', 'r_hand_thumb_1',
  // 'r_hand_index_2', 'r_hand_little_2', 'r_hand_middle_2', 'r_hand_ring_2', 'r_hand_thumb_2',
  'r_hand_index_3', 'r_hand_little_3', 'r_hand_middle_3', 'r_hand_ring_3', 'r_hand_thumb_3'
].forEach(linkName => {
  skinXML += `
  <gazebo reference="${linkName}">
    <selfCollide>true</selfCollide>
  </gazebo>`;
});

files.forEach(fileName => {
  // Skip files without config
  const configItem = config.bodyParts[fileName];
  if (!configItem) {
    return;
  }

  // Read file content
  const lines = fs.readFileSync(`${positionsPath}${fileName}`, 'utf-8').split('\n').map(str => str.trim());

  // Parse data
  let afterCalibration = false;
  const data = [];
  lines.forEach(line => {
    if (afterCalibration) {
      const numbers = line.split(/\s+/).map(x => Number(x));
      if (numbers.length === 6) {
        if (numbers.some(x => x !== 0)) {
          data.push(numbers);
        } else {
          data.push(null);
        }
      }
    }
    if (line === '[calibration]') {
      afterCalibration = true;
    }
  });

  // Generate joint
  skinXML += `
  <joint name="${configItem.joint.name}" type="${configItem.joint.type}">
    <origin
      xyz="${configItem.joint.origin.xyz[0]} ${configItem.joint.origin.xyz[1]} ${configItem.joint.origin.xyz[2]}"
      rpy="${configItem.joint.origin.rpy[0]} ${configItem.joint.origin.rpy[1]} ${configItem.joint.origin.rpy[2]}"
    />
    <parent link="${configItem.parentLink}" />
    <child link="${configItem.childLink}" />
    <limit effort="1" lower="0" upper="0" velocity="1" />
    <dynamics damping="${configItem.joint.dynamics.damping}" friction="${configItem.joint.dynamics.friction}" />
    <axis xyz="0 0 0" />
  </joint>`;

  // Generate child link with skin collision bodies
  skinXML += `
  <link name="${configItem.childLink}">
    <inertial>
      <origin xyz="0 0 0" rpy="0 0 0" />
      <mass value="1e-5" />
      <inertia ixx="1e-9" ixy="1e-9" ixz="1e-9" iyy="1e-9" iyz="1e-9" izz="1e-9" />
    </inertial>`;

  // Generate skin (collision bodies & visuals)
  data.forEach((taxel, index) => {
    if (!taxel) {
      return
    }
    const rpy = calcRpy(taxel.slice(3));
    skinXML += `
    <visual name="${configItem.childLink}_visual_${index}">
      <geometry>
        <!--cylinder length="${config.taxel.length}" radius="${config.taxel.radius}" /-->
        <sphere radius="${config.taxel.radius}" />
      </geometry>
      <origin xyz="${taxel[0]} ${taxel[1]} ${taxel[2]}" rpy="0 0 0" />
    </visual>
    <collision name="${configItem.childLink}_collision_${index}">
      <geometry>
        <!--cylinder length="${config.taxel.length}" radius="${config.taxel.radius}" /-->
        <sphere radius="${config.taxel.radius}" />
      </geometry>
      <origin xyz="${taxel[0]} ${taxel[1]} ${taxel[2]}" rpy="${taxel[3]} ${taxel[4]} ${taxel[5]}" />
    </collision>`;
  });

  // Add colors and textures here
  skinXML += `
  </link>
  <gazebo reference="${configItem.childLink}">
    <material>${configItem.color}</material>
  </gazebo>`;
});

// Contact detection model plugin
skinXML += `
  <gazebo>
    <plugin name="gazebo_icub_skin" filename="libcontact.so"></plugin>
  </gazebo>`;

// Inject skin code into model and output
const modelXML = fs.readFileSync(`${modelFileName}`, 'utf-8');
console.log(modelXML.replace('</robot>', skinXML + '\n</robot>'));
