#include <gazebo/gazebo.hh>
#include "Contact2Sensor.hh"

namespace gazebo {
  class RegisterContact2SensorPlugin : public SystemPlugin {
    public: virtual ~RegisterContact2SensorPlugin() {
    }

    public: void Load(int /*_argc*/, char ** /*_argv*/) {
      gazebo::sensors::RegisterContact2Sensor();
      printf("contact2 sensor loaded\n");
    }

    private: void Init() {
    }

  };

  // Register this plugin with the simulator
  GZ_REGISTER_SYSTEM_PLUGIN(RegisterContact2SensorPlugin)
}
