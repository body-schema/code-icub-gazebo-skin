# General imports
import numpy as np
import pickle
from operator import add

# ROS and Gazebo imports
from std_srvs.srv import Empty

# Explauto imports
from explauto import Environment
from explauto import SensorimotorModel, InterestModel
from explauto.sensorimotor_model import sensorimotor_models, available_configurations
from explauto.sensorimotor_model.non_parametric import NonParametric
from explauto.utils.config import make_configuration

# OpenAI gym imports
from icub_skin_env import IcubSkinEnv

# Utils
from functions import *

# # # # # # # # # # # # # # # # # # # # # # # # #

# OpenAI gym environment
env = IcubSkinEnv()
env.reset()

# Explauto configuration
explauto_config = make_configuration(
  env.observation_space['joints']['left_arm'].low[0:7],
  env.observation_space['joints']['left_arm'].high[0:7],
  [0, 0],
  [500, 500]
)

# Instantiate a random goal interest model:
im_model = InterestModel.from_configuration(explauto_config, explauto_config.s_dims, 'random')

# Explauto model
params = {'fwd': 'NN', 'inv': 'NN', 'sigma_explo_ratio':0.03}
model = NonParametric(explauto_config, **params)
# model = SensorimotorModel.from_configuration(explauto_config, 'nearest_neighbor', 'default')
model.mode = "explore" # "explore" or "exploit"

# Bootstrap model with home pose
model.update(home_pose()[0:7], [276, 306])
model.bootstrapped_s = True

for i in range(1000):
  # Generate goal
  s_g = im_model.sample()
  
  # Infer a motor command to reach that goal using the sensorimotor model:
  m = model.inverse_prediction(s_g)

  # Generate OpenAI gym action
  action = action_home(env)
  action['left_arm'][0:7] = np.double(np.array(m))

  # Perform action
  observation, reward, done, info = env.step(action)
  torso = observation['touch']['torso']
  
  # Update model, print status
  # print(reward, observation['touch']['torso'])
  print('Selected goal: ', [round(x, 2) for x in s_g])
  print('Motor command: ', [round(x, 2) for x in m])
  if (torso[0] + torso[1] > 0):
    model.update(m, torso)
    print('Observation: ', torso)
  else:
    print('Observation: None')
  print('')

  if (i > 0) and (i % 100 == 0):
    test_model(env, model, i)

