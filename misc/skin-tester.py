import yarp
import time

# Configuration
output_port_name = "/icubSim/skin/right_forearm_comp"
input_port_name =  "/skinGui/right_forearm:i"
n_taxels = 384

# Initialize YARP network
yarp.Network.init()

port = yarp.Port()
port.open(output_port_name)
yarp.Network.connect(output_port_name, input_port_name)

#for k in dir(port):
#  print(k)

for i in range(n_taxels):
#n = 348
#for i in range(n, n+1):
  print(i)
  data = yarp.Bottle()
  for j in range(n_taxels):
    data.addDouble(0 if i == j else 255)
  port.write(data)
  time.sleep(0.3);
