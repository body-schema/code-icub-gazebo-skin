const makePattern = 'black-bottom';

const lines = [
  0, 12, 24, 36, 48, 60, 72, 84, 96, 108, 120, 132, 144, 156, 168, 180,
  204, 252, 288, 300, 312, 336, 348
];

lines.sort((a, b) => a - b);
lines.forEach(lineNum => {
  for (let i = 0; i < 12; i++) {
    // console.log(`${lineNum + i} `);
  }
});

const producePattern = (pattern, i, x, y) => {
  const lines = pattern.split('\n').map(line => line.trim()).filter(x => x !== '').map(line => line.split(' ').map(Number));
  
  for (let j = 0; j < 12; j++) {
    const dx = lines[j][1] - lines[0][1];
    const dy = lines[j][2] - lines[0][2];
    console.log(`${i+j} ${x+dx} ${y+dy}`);
  }
};

// Black pattern
if (makePattern === 'black') {
  const patternBlack = `
    0 48 238
    1 48 260
    2 68 250
    3 68 228
    4 88 238
    5 106 228
    6 94 228
    7 88 216
    8 68 206
    9 48 194
    10 56 206
    11 48 216
  `;
  [
    [ 0, 48, 238],
    [24, 48, 128],
  ].forEach(([i, x, y]) => {
    producePattern(patternBlack, i, x, y);
  });
}

// Red pattern
if (makePattern === 'red') {
  const patternRed = `
    12 80 162
    13 60 172
    14 80 184
    15 98 172
    16 98 194
    17 118 206
    18 112 194
    19 118 184
    20 118 162
    21 118 140
    22 112 150
    23 98 150
  `;
  [
    [ 12,  80, 162],
    [ 84, 364, 106],
    [156, 176, 106],
  ].forEach(([i, x, y]) => {
    producePattern(patternRed, i, x, y);
  });
}

// White pattern
if (makePattern === 'white') {
  const patternWhite = `
    72 352 150
    73 332 140
    74 332 162
    75 352 172
    76 332 184
    77 332 206
    78 340 194
    79 352 194
    80 372 184
    81 390 172
    82 378 172
    83 372 162
  `;
  [
    [ 72, 352, 150],
    [ 96, 256,  96],
    [144, 164, 150],
  ].forEach(([i, x, y]) => {
    producePattern(patternWhite, i, x, y);
  });
}

// Green pattern
if (makePattern === 'green') {
  const patternGreen = `
    36 402 216
    37 402 195
    38 382 206
    39 382 228
    40 364 216
    41 344 228
    42 358 228
    43 364 238
    44 382 250
    45 402 260
    46 396 250
    47 402 238
  `;
  [
    [ 36, 402, 216],
    [ 60, 310, 272],
    [108, 310, 162],
  ].forEach(([i, x, y]) => {
    producePattern(patternGreen, i, x, y);
  });
}

// Blue pattern
if (makePattern === 'blue') {
  const patternBlue = `
    48 372 294
    49 390 282
    50 372 272
    51 352 284
    52 352 260
    53 332 250
    54 340 260
    55 332 272
    56 332 294
    57 332 314
    58 340 304
    59 352 304
  `;
  [
    [ 48, 372, 294],
    [120, 276, 238],
    [168, 184, 294],
  ].forEach(([i, x, y]) => {
    producePattern(patternBlue, i, x, y);
  });
}

// Orange pattern
if (makePattern === 'orange') {
  const patternOrange = `
    132 196 250
    133 215 260
    134 215 238
    135 196 228
    136 215 216
    137 215 195
    138 208 206
    139 196 206
    140 176 216
    141 156 228
    142 170 228
    143 176 238
  `;
  [
    [132, 196, 250],
    [180,  98, 304],
  ].forEach(([i, x, y]) => {
    producePattern(patternOrange, i, x, y);
  });
}

// Red (bottom) pattern
if (makePattern === 'red-bottom') {
  const patternRed = `
    204 88 374
    205 76 356
    206 98 356
    207 110 374
    208 120 356
    209 142 356
    210 132 362
    211 132 374
    212 120 394
    213 110 412
    214 110 400
    215 98 394
  `;
  [
    [204,  88, 374],
    [348, 142, 470],
  ].forEach(([i, x, y]) => {
    producePattern(patternRed, i, x, y);
  });
}

// Blue (bottom) pattern
if (makePattern === 'blue-bottom') {
  const patternBlue = `
    288 208 520
    289 186 520
    290 198 500
    291 220 500
    292 208 482
    293 220 464
    294 220 476
    295 230 482
    296 240 500
    297 252 520
    298 240 514
    299 230 520
  `;
  [
    [288, 208, 520],
    [336, 154, 424],
  ].forEach(([i, x, y]) => {
    producePattern(patternBlue, i, x, y);
  });
}

// Black (bottom) pattern
if (makePattern === 'black-bottom') {
  const patternBlack = `
    252 340 394
    253 328 412
    254 316 394
    255 328 374
    256 306 374
    257 296 356
    258 306 362
    259 316 356
    260 340 356
    261 362 356
    262 350 362
    263 350 374
  `;
  [
    [252, 340, 394],
    [300, 284, 490],
  ].forEach(([i, x, y]) => {
    producePattern(patternBlack, i, x, y);
  });
}
