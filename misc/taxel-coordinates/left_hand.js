const makePattern = 'black-bottom';

const lines = [
  0, 12, 24, 36, 48,
  96, 108, 120, 132
];

lines.sort((a, b) => a - b);
lines.forEach(lineNum => {
  for (let i = 0; i < 12; i++) {
    console.log(`${lineNum + i} `);
  }
});
