import yarp
import time

N_TAXELS_TORSO = 768
N_TAXELS_ARM = 768
N_TAXELS_FOREARM = 384
N_TAXELS_HAND = 192

class ExampleReader:
  BODY_PARTS = [
    [N_TAXELS_ARM, "/gym_icub_skin/left_arm:i", "/icubSim/skin/left_arm_comp"],
    [N_TAXELS_FOREARM, "/gym_icub_skin/left_forearm:i", "/icubSim/skin/left_forearm_comp"],
    [N_TAXELS_HAND, "/gym_icub_skin/left_hand:i", "/icubSim/skin/left_hand_comp"],
    [N_TAXELS_ARM, "/gym_icub_skin/right_arm:i", "/icubSim/skin/right_arm_comp"],
    [N_TAXELS_FOREARM, "/gym_icub_skin/right_forearm:i", "/icubSim/skin/right_forearm_comp"],
    [N_TAXELS_HAND, "/gym_icub_skin/right_hand:i", "/icubSim/skin/right_hand_comp"],
    [N_TAXELS_TORSO, "/gym_icub_skin/torso:i", "/icubSim/skin/torso_comp"]
  ]
  
  input_ports = []

  def __init__(self):
    yarp.Network.init()

    # Initialize skin ports
    for bp in self.BODY_PARTS:
      port = yarp.BufferedPortBottle()
      port.open(bp[1])
      yarp.Network.connect(bp[2], bp[1])
      self.input_ports.append(port);

    # Initialize motors
    props = yarp.Property()
    props.put("device", "remote_controlboard")
    props.put("local", "/client/left_arm")
    props.put("remote","/icubSim/left_arm")
    self.armDriver = yarp.PolyDriver(props)
    
    #query motor control interfaces
    self.iPos = self.armDriver.viewIPositionControl()
    self.iVel = self.armDriver.viewIVelocityControl()
    self.iEnc = self.armDriver.viewIEncoders()

    # Retrieve number of joints
    numJoints = self.iPos.getAxes()
    print('Controlling', numJoints, 'joints')
    
    # Read encoders
    encs = yarp.Vector(numJoints)
    self.iEnc.getEncoders(encs.data())
    print("Encoders:", [encs[i] for i in range(numJoints)])
    # for i in range(numJoints):
    #   print(encs[i]);
    
    # Store as home position
    self.homePose = yarp.Vector(numJoints, encs.data())

    # Set movement speed
    speeds = yarp.Vector(numJoints)
    for i in range(numJoints):
      speeds[i] = 100;
    self.iPos.setRefSpeeds(speeds.data())
    
    # Move some joints
    tmp = yarp.Vector(numJoints, encs.data())
    tmp[0] = 0
    self.iPos.positionMove(tmp.data())
    for i in range(50):
      time.sleep(0.1)
      encs = yarp.Vector(numJoints)
      self.iEnc.getEncoders(encs.data())
      v = [encs[i] for i in range(numJoints)]
      print(v)
    
    return

  def getData(self):
    btl = self.input_ports[0].read(True) # True for blocking

    btl_size = btl.size();
    my_data = btl.get(0).asDouble()

    return my_data
    
if __name__ == "__main__":
  reader = ExampleReader()
  print(reader.getData())

